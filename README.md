# grafana-tf-ec2



## Preparations

1. Create an AWS IAM account with programmatic access enabled and EC2FullAccess (or similar policy) attached using the AWS Console. Download the credentials.

## Provisioning
1. Specify your AWS creds and keypair name in terraform.tfvars file. The variable names are access_key, secret_key and keypair_name.

note: you can also override other default values if needed. 

2. Run the following commands to provision an instance:

```
terraform init
terraform plan
terraform apply

```
## Access Grafana

1. Open (ElasticIP from Terraform output):3000 to access Grafana web interface. Default creds are admin:admin, password change is required upon first login.
2. In order to use a custom dashboard (e.g AWS CloudWatch Browser), we can add a Cloudwatch datasource using IAM creds generated before (a corresponding policy should be attached). Then we can import a dashboard from a library using it's id (590).
