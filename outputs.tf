output "id" {
  value = aws_instance.grafana[0].id
}
output "elastic_ip" {
  value = aws_eip.grafana.public_ip
}
output "pubkey" {
  value = tls_private_key.sshkey.public_key_openssh
}