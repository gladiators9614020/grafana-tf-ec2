provider "aws" {
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
  region = "${var.aws_region}"
}

resource "tls_private_key" "sshkey" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "grafana" {
  key_name   = "${var.keypair_name}"
  public_key = tls_private_key.sshkey.public_key_openssh
}

resource "aws_security_group" "grafana_sg" {
  name_prefix = "grafana_sg"
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 3000
    to_port     = 3000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
resource "aws_instance" "grafana" {
  ami           = "${var.ami}"
  count         = 1
  instance_type = "${var.instance_type}"
  key_name      = aws_key_pair.grafana.key_name
  vpc_security_group_ids = [aws_security_group.grafana_sg.id]
  tags = {
    Name = "${var.instance_name}"
  }
}
resource "aws_eip" "grafana" {
  instance = aws_instance.grafana[0].id
}

resource "null_resource" "execute_commands" {
  depends_on = [
    aws_eip.grafana
  ]

  provisioner "remote-exec" {
    inline = var.commands
  }

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = tls_private_key.sshkey.private_key_pem
    host        = aws_eip.grafana.public_ip
  }
}
