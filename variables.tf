variable "instance_type" {
  default = "t2.micro"
}
variable "ami" {
  default = "ami-007855ac798b5175e"
}
variable "aws_region" {
  default = "us-east-1"
}
variable "keypair_name" {
  default = "grafana"
}
variable "instance_name" {
  default = "grafana"
}
variable "access_key" {
}
variable "secret_key" {
}
variable "commands" {
  type = list(string)
  default = ["curl -fsSL https://get.docker.com -o get-docker.sh",
    "sudo sh get-docker.sh",
    "sudo chmod 666 /var/run/docker.sock",
    "docker run -d -v datasources:/etc/grafana/provisioning/datasources -v grafana-data:/var/lib/grafana -p 3000:3000 grafana/grafana:latest"]
}
